#include "DoubleLinkedList.h"

void DoubleLinkedList::PushTop(int value) {
	
	Node* newNode = new Node(value);

	if (head == nullptr)
	{
		head = tail = newNode;
	}
	else {
		newNode->next = head;
		head->prev = newNode;
		head = newNode;
	}
}

bool DoubleLinkedList::EraseElementByPointer(Node* pointer) {
	if (head == nullptr)
	{
		return false;
	}

	if (pointer == head)
	{
		Node* temp = head;
		head = head->next;
		if (head != nullptr)
		{
			head->prev = nullptr;
		}
		else {
			tail = nullptr;
		}
		delete temp;
		return true;
	}

	if (pointer == tail)
	{
		Node* temp = tail;
		tail = tail->prev;
		tail->next = nullptr;
		delete temp;
		return true;
	}

	Node* current = head->next;
	while (current != nullptr && current != tail)
	{
		if (current == pointer)
		{
			current->prev->next = current->next;
			current->next->prev = current->prev;
			delete current;
			return true;
		}
		current = current->next;
	}
	return false;
}

size_t DoubleLinkedList::size() {
	size_t count = 0;
	Node* current = head;
	while (current != nullptr)
	{
		++count;
		current = current->next;
	}
	return count;
}

bool DoubleLinkedList::isEmpty() {
	return head == nullptr;
}