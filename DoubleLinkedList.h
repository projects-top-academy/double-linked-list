#pragma once
#include "Node.h"
#include<iostream>


class DoubleLinkedList
{
public:

	DoubleLinkedList() : head(nullptr), tail(nullptr) {}

	void PrintValuesInList() const {
		std::cout << "\n" << "Values in the list: ";
		Node* current = head;
		while (current != nullptr) {
			std::cout << current->value << " ";
			current = current->next;
		}
		std::cout << "\n";
	}

	void PushBack(int value) {
		Node* newNode = new Node(value);
		if (head == nullptr)
		{
			head = tail = newNode;
		}
		else {
			tail->next = newNode;
			newNode->prev = tail;
			tail = newNode;
		}
	}

	int GetLastValue() {
		if (tail == nullptr)
		{
			return -1;
		}
		return tail->value;
	}

	Node* GetNodeByValue(int targetValue) {
		Node* current = head;

		while (current != nullptr)
		{
			if (current->value == targetValue)
			{
				return current;
			}
			current = current->next;
		}
		return nullptr;
	}

	void PushTop(int value);

	bool EraseElementByPointer(Node* pointer);

	size_t size();

	bool isEmpty();

private:
	Node* head;
	Node* tail;
};