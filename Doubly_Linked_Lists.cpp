﻿#include <iostream>
#include "DoubleLinkedList.h"

int main()
{
    DoubleLinkedList myList;
    myList.PushBack(1);
    myList.PushBack(3);
    myList.PushBack(5);
    myList.PrintValuesInList();

    myList.PushTop(7);
    myList.PrintValuesInList();

    Node* nodeToFind = myList.GetNodeByValue(3);
    if (nodeToFind != nullptr)
    {
        myList.EraseElementByPointer(nodeToFind);
        myList.PrintValuesInList();
    }
    else {
        std::cout << "Element with value 3 was not found.\n";
    }

    std::cout << "Size of the list: " << myList.size() << "\n";
    std::cout << "Is the list empty? " << (myList.isEmpty() ? "Yes" : "No") << "\n";

    return 0;
}

