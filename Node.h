#pragma once
class Node
{
public:
	int value;
	Node* next;
	Node* prev;

	Node(int val) : value(val), next(nullptr), prev(nullptr) {}
};
